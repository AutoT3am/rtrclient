package support.classes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by beckmambetov on 8/8/2016.
 */
public class GitRunner {

    public static void downloadFramework(RunConfiguration rc) throws IOException {
        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", "git clone " + rc.URL);
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line = "";
        while (p.isAlive()) {
            line = r.readLine();
            if (line != null && line.contains("fatal: destination path") && line.contains("already exists and is not an empty directory")) {
                p.destroy();
                r.close();
                line = line.replace("fatal: destination path '", "").replace("' already exists and is not an empty directory.", "");
                builder = new ProcessBuilder(
                        "cmd.exe", "/c", "rmdir " + line + " /s /q");
                p = builder.start();
                while (p.isAlive()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                p.destroy();
                builder = new ProcessBuilder(
                        "cmd.exe", "/c", "git clone " + rc.URL);
                builder.redirectErrorStream(true);
                p = builder.start();
                r = new BufferedReader(new InputStreamReader(p.getInputStream()));
            }
        }
        p.destroy();
        r.close();
    }
}
