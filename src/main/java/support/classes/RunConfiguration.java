package support.classes;

/**
 * Created by beckmambetov on 8/5/2016.
 */
public class RunConfiguration {

    public String URL;
    public String VCS;
    public String tag;
    public String user;
    public String pass;

    public RunConfiguration(String URL, String VCStype, String tagtoRun, String user, String pass){
        this.URL = URL;
        this.VCS = VCStype;
        this.tag = tagtoRun;
        this.user = user;
        this.pass = pass;

    }

}
