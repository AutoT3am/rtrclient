package ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import java.io.IOException;

//Service Endpoint Interface
@WebService
@SOAPBinding(style = Style.RPC)
public interface RunnerService {

    @WebMethod
    public void runTests(@WebParam(name="URL") String URL,@WebParam(name="VCStype") String VCStype,@WebParam(name="tagToRun") String tagtoRun,@WebParam(name="user") String user,@WebParam(name="pass") String pass) throws IOException;



}