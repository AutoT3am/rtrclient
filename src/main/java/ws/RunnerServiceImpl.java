package ws;

import support.classes.GitRunner;
import support.classes.RunConfiguration;

import javax.jws.WebService;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

//Service Implementation
@WebService(endpointInterface = "ws.RunnerService")
public class RunnerServiceImpl implements RunnerService {

    @Override
    public void runTests(String URL, String VCStype, String tagtoRun, String user, String pass) throws IOException {
        RunConfiguration rc = new RunConfiguration(URL, VCStype, tagtoRun, user, pass);
        switch (rc.VCS.toLowerCase()) {
            case "git": {
                GitRunner.downloadFramework(rc);
                break;
            }
            case "svn": {

            }
        }
        setupBoundaries(rc);
        startFramework(rc);
    }

    public static void startFramework(RunConfiguration rc) throws IOException {
        String line = rc.URL.substring(rc.URL.lastIndexOf('/') + 1, rc.URL.lastIndexOf('.'));
        ProcessBuilder builder = new ProcessBuilder(
                "cmd.exe", "/c", "cd " + line + "&& mvn test");
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        while (p.isAlive()) {
            try {
                System.out.println(r.readLine());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void setupBoundaries(RunConfiguration rc) throws IOException {
        String line = rc.URL.substring(rc.URL.lastIndexOf('/') + 1, rc.URL.lastIndexOf('.'));
        File runner = new File(RunnerServiceImpl.class.getProtectionDomain().getCodeSource().getLocation().getPath().split("RTRClient")[0] + "RTRClient"
                + File.separator + line + File.separator+ "src" + File.separator + "test" + File.separator + "java" + File.separator + "testUtils"+File.separator + "TestRunner.java");

        List<String> text = new ArrayList<>();
        String temp = "";
        try (BufferedReader r = new BufferedReader(new FileReader(runner));){
            while (temp != null) {
                if (temp.contains(", tags = {")) {
                    temp = temp.split(", tags = \\{")[0] + ", tags = {\"" + rc.tag  + "\""+ temp.split("\".*?\"")[1];
                }
                text.add(temp);
                temp = r.readLine();
            }
        }
        BufferedWriter w = new BufferedWriter(new FileWriter(runner));
        try {
            for (String x : text) {
                w.write(x);
                w.newLine();
            }
            w.flush();
        } finally {
            w.close();
        }
        }

    }