package ws;

import javax.xml.ws.Endpoint;

//Endpoint publisher
public class EndpointPublisher {

    public static void main(String[] args) {
        Endpoint.publish("http://localhost:9999/ws/rtr", new RunnerServiceImpl());
        System.out.println("Service started");
    }

}